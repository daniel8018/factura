package daniel.rivera.ui;

import daniel.rivera.bl.Cliente;
import daniel.rivera.bl.Factura;
import daniel.rivera.bl.Producto;
import daniel.rivera.dl.Controller;
import java.io.*;
import java.time.LocalDate;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Main {


    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller admin = new Controller();
    static Producto product = new Producto();

    public static void main(String[] args) throws IOException {
        crearMenu();
    }

    public static void crearMenu() throws IOException {
        int opcion = 0;
        do {
            out.println("1. Registrar Cliente");
            out.println("2. Lista de Clientes");
            System.out.println("");
            out.println("3. Registrar Producto");
            out.println("4. Lista de Produtos");
            System.out.println("");
            out.println("5. Registrar Factura");
            out.println("6. Listar Facturas");
            System.out.println("");
            out.println("7. Agregar linea a factura");
            out.println("8. Salir");
            System.out.println("");
            out.print("Digite el número de opción: ");
            System.out.println("");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        } while (opcion != 8);
    }

    public static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion) {
            case 1:
                registrarCliente();
                break;
            case 2:
                mostrarClientes();
                break;
            case 3:
                registrarProducto();
                break;
            case 4:
                mostrarProductos();
                break;
            case 5:
                registrarFactura();
                break;
            case 6:
                imprimirFactura();
                break;
            case 7:
                agregarLineaFactura();
                break;
            case 8:
                out.println("OUT");
                break;
            default:
                out.println("Valor inválido");
                break;
        }
    }

    //__________________________________________________________________
    public static void mostrarClientes() throws IOException {
        String[] datos = admin.listarClientes();
        for (String factura : datos) {
            out.println(factura);
        }
    }

    public static void mostrarProductos() throws IOException {
        String[] datos = admin.mostrarProducto();
        for (String producto : datos) {
            out.println(producto);
        }
    }

    public static void imprimirFactura() throws IOException {
        System.out.print("Ingrese el número de la factura a imprimir: ");
        String numeroFactura = in.readLine();
        int posFactura = admin.buscarPosfactura(numeroFactura);

        String[] datos = admin.mostrarFactura();
        String datosFactura = datos[posFactura].toString();
        out.println(datosFactura + "\n");

        out.println("------------------------------------");
        out.println("Subtotal: " + admin.calcularSubTotal(numeroFactura));
        out.println("------------------------------------");
        out.println("Impuestos 13%: " + admin.calcularImpuesto(numeroFactura));
        out.println("------------------------------------");
        out.println("TOTAL: " + admin.calcularTotal(numeroFactura));
    }

//__________________________________________________________________


    public static void registrarCliente() throws IOException {
        Cliente client = new Cliente();
        out.println("Ingrese la identificacion del cliente");
        String identificacion = in.readLine();
        out.println("Ingrese el genero, M o F");
        String genero = in.readLine();
        out.println("Ingrese el nombre del cliente");
        String nombre = in.readLine();
        out.println("Ingrese el dia de nacimiento del cliente");
        int dia = Integer.parseInt(in.readLine());
        out.println("Ingrese el mes de nacimiento del cliente");
        int mes = Integer.parseInt(in.readLine());
        out.println("Ingrese el año de nacimiento del cliente");
        int year = Integer.parseInt(in.readLine());
        LocalDate fechaNacimiento = LocalDate.of(year, mes, dia);
        int edad = admin.CalcularEdad(year);
        client.setEdad(edad);
        if (admin.encontrarCliente(identificacion) == null) {
            String resultado = admin.registrarCliente(nombre, identificacion, genero, fechaNacimiento, edad);
        } else {
            out.println("El cliente de identificación" + identificacion + " ya está registrado!");
        }

    }

    public static void registrarProducto() throws IOException {

        out.println("Ingrese el codigo del producto");
        String codigo = in.readLine();
        out.println("Ingrese la descripcion");
        String descripcion = in.readLine();
        out.println("Ingrese el precio");
        Double precio = Double.parseDouble(in.readLine());
        if (admin.encontrarProducto(codigo) == null) {
            String resultado = admin.registrarProducto(codigo, descripcion, precio);
        } else {
            out.println("El producto con el codigo" + codigo + " ya¡ existe !");
        }

    }


    public static void registrarFactura() throws IOException {
        out.println("Ingrese la cedula del cliente");
        String cedula = in.readLine();
        out.println("Ingrese el numero que desea asignar a la factura");
        String numero = in.readLine();
        out.println("Ingrese el dia de creacion de la factura");
        int dia = Integer.parseInt(in.readLine());
        out.println("Ingrese el mes  de creacion de la factura");
        int mes = Integer.parseInt(in.readLine());
        out.println("Ingrese el año de creacion de la factura");
        int year = Integer.parseInt(in.readLine());
        LocalDate fechaFactura = LocalDate.of(year, mes, dia);
        if (admin.encontrarCliente(cedula) != null) {
            if (admin.encontrarFactura(numero) == null) {
                String resultado = admin.registrarFactura(numero, cedula, fechaFactura);
                out.println(resultado);
            } else {
                out.println("La factura numero: " + numero + " no se encuentra");
            }
        } else {
            out.println("El cliente cedula: " + cedula + " no se encuentra");
        }
        out.println("La factura se ha registrado correctamente");
    }

    //__________________________________________________________________
    public static void agregarLineaFactura() throws IOException {
        System.out.print("Ingrese el numero de la factura: ");
        String numero = in.readLine();
        out.println("Ingrese el numero de la linea: ");
        int numeroLinea = Integer.parseInt(in.readLine());
        out.println("Ingrese la cantidad de productos de la factura:  ");
        double cantidad = Double.parseDouble(in.readLine());
        System.out.print("Ingrese el codigo del producto previamente ingresado: ");
        String codigo = in.readLine();

        if (admin.encontrarFactura(numero) != null) {
            if (admin.encontrarProducto(codigo) != null) {
                String respuesta = admin.agregarLinea(numero, numeroLinea, cantidad, codigo);
                out.println(respuesta);
            } else {
                out.println("El producto codigo" + codigo + " no se encuentra");
            }
        } else {
            out.println("La factura con numero" + numero + " no se encuentra");
        }
    }
    //__________________________________________________________________
}
