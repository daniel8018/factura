package daniel.rivera.dl;


import daniel.rivera.bl.Cliente;
import daniel.rivera.bl.Factura;
import daniel.rivera.bl.Linea;
import daniel.rivera.bl.Producto;
import daniel.rivera.tl.CapaLogica;

import java.time.LocalDate;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Controller {
    private CapaLogica logica = new CapaLogica();



    /**
     * metodo utilizado para registrar el objeto Cliente y ser enviado a la capa lógica
     * @param cedula Variable que simboliza la cedula de la clase Cliente
     * @param nombre Variable que simboliza el nombre de la clase Cliente
     * @param genero Variable que simboliza el genero de la clase Cliente
     * @param fechaNacimiento Variable que simboliza la fecha de nacimiento del cliente de la clase Cliente
     * @param edad Variable que simboliza la edad de la clase Cliente
     * @return la variable de retorno simboliza el cliente como objeto
     */
    public String registrarCliente(String cedula, String nombre, String genero, LocalDate fechaNacimiento, int edad) {
        Cliente cliente = new Cliente(cedula, nombre, genero, fechaNacimiento, edad);
        return logica.registrarCliente(cliente);
    }

    /**
     * metodo utilizado para registrar el objeto Producto y ser enviado a la capa logica
     * @param codigo Variable que simboliza el codigo de la clase Producto
     * @param descripcion Variable que simboliza la descripción de la clase Producto
     * @param precio Variable que simboliza el precio de la clase Producto
     * @return la variable de retorno simboliza el objeto producto
     */
    public String registrarProducto(String codigo, String descripcion, Double precio) {
        Producto producto = new Producto(codigo, descripcion, precio);
        return logica.registrarProducto(producto);
    }

    /**
     * metodo utilizado para registrar el objeto Factura y ser enviado a la capa Logica
     * @param numero Variable que simboliza el numero de la clase factura
     * @param cedula Variable que simboliza la cedula de la clase factura
     * @param fechaFactura Variable que simboliza la fecha de la clase factura
     * @return la variable de retorno simboliza el objeto de factura
     */
    public String registrarFactura(String numero, String cedula, LocalDate fechaFactura) {
        Cliente cliente = logica.encontrarCliente(cedula);
        if (cliente != null) {
            Factura factura = new Factura(numero, fechaFactura, cliente);
            return logica.registrarFacturas(factura);
        } else {
            return "El cliente ced: " + cedula + " no se encuentra";
        }

    }

    //__________________________________________________________________

    /**
     * metodo utilizado para registrar el objeto factura y ser enviado a la capa logica
     * @param numero Variable que simboliza la factura de la clase factura
     * @return la variable de retorno simboliza el numero de la factura
     */
    public Factura encontrarFactura(String numero) {
        return logica.encontrarFactura(numero);
    }

    /**
     * metodo utilizado para registrar el objeto Cliente y ser enviado a la capa logica
     * @param cedula Variable que simboliza la cedula de la clase factura
     * @return la variable de retorno simboliza la cedula del cliente
     */
    public Cliente encontrarCliente(String cedula) {
        return logica.encontrarCliente(cedula);
    }

    /**
     * metodo utilizado para registrar el objeto Producto y ser enviado a la capa logica
     * @param codigo Variable que simboliza el codigo de la clase factura
     * @return la variable de retorno simboliza el producto
     */
    public Producto encontrarProducto(String codigo) {
        return logica.encontrarProducto(codigo);
    }

  //__________________________________________________________________

    /**
     * método que envia los valores del cliente a la capa logica
     * @return la variable de retorno simboliza el objeto de clientes
     */
    public String[] listarClientes() {
        return logica.listarClientes();
    }

    /**
     * método que envia los valores del Producto a la capa logica
     * @return la variable de retorno simboliza el objeto de Producto
     */
    public String[] mostrarProducto() {
        return logica.mostrarProducto();
    }

    /**
     * método que envia los valores de factura a la capa logica
     * @return la variable de retorno simboliza el objeto de Factura
     */
    public String[] mostrarFactura() {
        return logica.mostrarFactura();
    }

    //__________________________________________________________________

    /**
     * metodo utilizado para registrar el objeto Linea y ser enviado a la capa logica
     * @param numero Variable que simboliza el numero de la clase Factura
     * @param numeroLinea Variable que simboliza el numero de Línea
     * @param cantidad Variable que simboliza la cantidad de lineas de la factura
     * @param codigo Variable que simboliza el codigo de la clase Factura
     * @return la variable de retorno simboliza el resultado, si se agrego la linea o no
     */
    public String agregarLinea(String numero, int numeroLinea, double cantidad, String codigo) {
        Factura factura = logica.encontrarFactura(numero);
        if (factura != null) {
            Producto prod = logica.encontrarProducto(codigo);
            if (prod != null) {
                factura.agregarLinea(numeroLinea, cantidad, prod);
                return "La linea se agrego exitosamente";
            } else {
                return "El producto  " + codigo + " no existe";
            }
        }
        return "La factura buscada no existe";
    }

    /**
     * metodo que tiene la funcion de buscar la posicion de la factura
     * @param numeroFactura Variable que simboliza el numero de la clase factura
     * @return la variable de retorno simboliza
     */
    public int buscarPosfactura(String numeroFactura) {
        int encontrada = logica.buscarPosFactura(numeroFactura);
        return encontrada;
    }

    /**
     * metodo que calcula el impuesto
     * @param numeroFactura es el numero de la factura
     * @return retorna el numero de la factura
     */
    public double calcularImpuesto(String numeroFactura) {
        return logica.calcularSubTotal(numeroFactura) * 13 / 100;
    }

    /**
     * metodo que calcula el total del precio
     * @param numeroFactura el numero de factura
     * @return la variable de retorno simboliza el calculo enviado a la capa logica
     */
    public double calcularTotal(String numeroFactura) {

        return logica.calcularSubTotal(numeroFactura) + calcularImpuesto(numeroFactura);
    }

    /**
     * metodo que calcula el subtotal de la factura
     * @param numeroFactura Variable que simboliza el subtotal de la clase factura
     * @return la variable de retorno simboliza el subtotal y lo envia a la capa logica
     */
    public double calcularSubTotal(String numeroFactura) {
        return logica.calcularSubTotal(numeroFactura);
    }

    /**
     * metodo que calcula la edad según la fecha de nacimiento brindada
     * @param year Variable que simboliza el anio de la clase cliente
     * @return la variable de retorno simboliza la edad calculada
     */
    public int CalcularEdad(int year) {
        int edad = 0, actualYear = LocalDate.now().getYear();
        edad = (actualYear - year);
        return edad;
    }
  //__________________________________________________________________
}