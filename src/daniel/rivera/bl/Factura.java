package daniel.rivera.bl;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Factura {
    private String numero;
    private Cliente cliente;
    private LocalDate fecha_Emision;
    private  ArrayList<Linea> lines;
    private ArrayList<Producto> product;

    /**
     * Constructor vacio para la clase Factura
     */
    public Factura() {
    }

    /**
     * Constructor que recibe los parametros de parámetros de factura y los Inicializa
     * @param numero Variable que simboliza el numero de la clase factura
     * @param fecha_Emision Variable que simboliza la fecha de emision de la clase factura
     */
    public Factura(String numero, LocalDate fecha_Emision) {
        this.numero = numero;
        this.fecha_Emision = fecha_Emision;
    }

    /**
     * Constructor que recibe todos los parámetros de factura y el objeto cliente y los Inicializa
     * @param numero Variable que simboliza el numero de la factura de la clase factura
     * @param fecha_Emision Variable que simboliza la fecha de emision de la clase factura
     * @param cliente Variable que simboliza el objeto cliente de la clase factura
     */
    public Factura(String numero, LocalDate fecha_Emision, Cliente cliente) {
        this.numero = numero;
        this.fecha_Emision = fecha_Emision;
        this.cliente = cliente;
        this.product= new ArrayList<>();
        this.lines = new ArrayList<>();
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada de numero de la clase factura
     * @return la variable de retorno simboliza el numero de factura
     */
    public String getNumero() {
        return numero;
    }

    /**
     * metodo utilizado para obtener los datos del Arraylist Linea privado de la clase
     * @return la variable de retorno simboliza los valores del array
     */
    public ArrayList<Linea> getLines() {
        return lines;
    }

    /**
     * metodo utilizado para asignar valor a  los datos del Arraylist privado de la clase Factura
     * @param lines
     */
    public void setLines(ArrayList<Linea> lines) {
        this.lines = lines;
    }

    /**
     * Metodo utilizado para modificar el atributo privado numero de la clase factura
     * @param numero Variable que simboliza el numero de la clase Factura
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     *  metodo utilizado para obtener los datos del Arraylist product privado de la clase Factura
     * @return la variable de retorno simboliza los valores del Arraylist
     */
    public ArrayList<Producto> getProduct() {
        return product;
    }

    /**
     * Metodo utilizado para modificar el atributo privado producto de la clase Factura
     * @param product Variable que simboliza los valores del arraylist de la clase Factura
     */
    public void setProduct(ArrayList<Producto> product) {
        this.product = product;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Cliente de la clase Factura
     * @return la variable de retorno simboliza el cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * Metodo utilizado para modificar el objeto privado Cliente de la clase Factura
     * @param cliente Variable que simboliza el objeto cliente de la clase Factura
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * metodo utilizado para registrar el objeto Cliente y ser enviado a la Capalogica
     * @param nombre Variable que simboliza el nombre de la clase Cliente
     * @param cedula Variable que simboliza la cedula del Cliente
     * @param genero Variable que simboliza el genero del Cliente
     * @param fechaNacimiento Variable que simboliza la fecha de nacimiento del Cliente
     * @param edad Variable que simboliza la edad del Cliente
     */
    public void agregarCliente(String nombre, String cedula, String genero, LocalDate fechaNacimiento, int edad) {
        Cliente client = new Cliente(nombre, cedula, genero, fechaNacimiento, edad);
    }

    /**
     * metodo utilizado para registrar el objeto producto y ser creado uno nuevo
     * @param codigo Variable que simboliza el codigo de la clase Factura
     * @param descripcion Variable que simboliza la descripcion de la clase Factura
     * @param precio Variable que simboliza el precio de la clase Factura
     */
    public void agregarProducto(String codigo, String descripcion, double precio) {
        Producto product = new Producto(codigo,descripcion,precio);
    }

    /**
     * metodo utilizado para registrar el objeto producto y lo agrega al arraylist
     * @param numeroLinea Variable que simboliza el numero de la linea de la clase Linea
     * @param cantidad Variable que simboliza la cantidad de la clase producto
     * @param producto Variable que simboliza el objeto producto  de la clase producto
     */
    public void agregarLinea(int numeroLinea, double cantidad, Producto producto){
        Linea line = new Linea(numeroLinea,cantidad,producto);
        lines.add(line);
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha de emision de la clase Factura
     * @return la variable de retorno simboliza la fecha de emision
     */
    public LocalDate getFechaEmision() {
        return fecha_Emision;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha de emision de la clase Factura
     * @param fechaEmision Variable que simboliza la fecha de emision de la clase factura
     */
    public void setFechaEmision(LocalDate fechaEmision) {
        this.fecha_Emision = fechaEmision;
    }

     /**
          * Metodo utilizado para imprimir todos los atributos de la clase - en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase - en unico string
          */
    public String toString() {
        return "Factura{" +
                "numero='" + numero + '\'' +
                ", fechaEmision=" + fecha_Emision  +"\n"+
                "----------------------------------"+"\n"+
                " cliente=" + cliente +"\n"+
                "----------------------------------"+"\n"+
                " linea/info=" + lines +
                '}';
    }

}