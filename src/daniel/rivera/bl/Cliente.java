package daniel.rivera.bl;

import java.time.LocalDate;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Cliente {
    private int edad;
    private String nombre;
    private String cedula;
    private String genero;
    private LocalDate fecha_Nacimiento;


    /**
     * Constructor vacio para la clase Cliente
     */
    public Cliente() {
    }

    /**
     * Constructor que recibe todos los parámetros de  Cliente  y los Inicializa
     * @param nombre Variable que simboliza el nombre de la clase cliente
     * @param cedula Variable que simboliza la cedula de la clase cliente
     * @param genero Variable que simboliza el genero de la clase cliente
     * @param fecha_Nacimiento Variable que simboliza la fecha de la clase cliente
     * @param edad Variable que simboliza la edad de la clase cliente
     */
    public Cliente(String nombre, String cedula, String genero, LocalDate fecha_Nacimiento, int edad) {
        this.edad = edad;
        this.nombre = nombre;
        this.cedula = cedula;
        this.genero = genero;
        this.fecha_Nacimiento = fecha_Nacimiento;

    }

    /**
     * metodo utilizado para obtener el valor de la variable privada nombre de la clase Cliente
     * @return la variable de retorno simboliza el nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo utilizado para modificar el atributo privado nombre de la clase Cliente
     * @param nombre Variable que simboliza el nombre de la clase Cliente
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada cedula de la clase Cliente
     * @return la variable de retorno simboliza la cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * Metodo utilizado para modificar el atributo privado cedula de la clase Cliente
     * @param cedula Variable que simboliza la cedula de la clase cliente
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada genero de la clase Cliente
     * @return la variable de retorno simboliza el genero del cliente
     */
    public String getGenero() {
        return genero;
    }

    /**
     * Metodo utilizado para modificar el atributo privado genero de la clase Cliente
     * @param genero Variable que simboliza el genero de la clase Cliente
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada fecha de nacimiento  de la clase Cliente
     * @return la variable de retorno simboliza la fecha de nacimiento
     */
    public LocalDate getFechaNacimiento() {
        return fecha_Nacimiento;
    }

    /**
     * Metodo utilizado para modificar el atributo privado fecha de nacimiento de la clase Cliente
     * @param fecha_Nacimiento Variable que simboliza la fecha de nacimiento de la clase Cliente
     */
    public void setFechaNacimiento(LocalDate fecha_Nacimiento) {
        this.fecha_Nacimiento = fecha_Nacimiento;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada Edad de la clase Cliente
     * @return la variable de retorno simboliza la edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Metodo utilizado para modificar el atributo privado edad de la clase Cliente
     * @param edad
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }


     /**
          * Metodo utilizado para imprimir todos los atributos de la clase Cliente en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase Cliente en unico string
          */
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                ", cedula='" + cedula + '\'' +
                ", genero='" + genero + '\'' +
                ", fechaNacimiento=" + fecha_Nacimiento +
                ", edad=" + edad +
                '}';
    }
}