package daniel.rivera.bl;

/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Linea {
    private int numLinea;
    private double cantidad;
    private Producto producto;

    /**
     * Constructor vacio para la clase Linea
     */
    public Linea() {
    }

    /**
     * Constructor que recibe todos los parámetros de  Linea  y los Inicializa
     * @param numLinea Variable que simboliza el numero de linea de la clase Linea
     * @param cantidad Variable que simboliza la cantidad de productos de la clase producto
     * @param producto Variable que simboliza el producto de la clase producto
     */
    public Linea(int numLinea, double cantidad, Producto producto) {
        this.numLinea = numLinea;
        this.cantidad = cantidad;
        this.producto = producto;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada numero de linea de la clase Linea
     * @return la variable de retorno simboliza el numero de linea
     */
    public int getnumLinea() {
        return numLinea;
    }

    /**
     * Metodo utilizado para modificar el atributo privado numero de linea de la clase Linea
     * @param numLinea Variable que simboliza el numero de linea de la clase Linea
     */
    public void setnumLinea(int numLinea) {
        this.numLinea = numLinea;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada cantidad de la clase Linea
     * @return la variable de retorno simboliza la cantidad de producto
     */
    public double getCantidad() {
        return cantidad;
    }

    /**
     * Metodo utilizado para modificar el atributo privado cantidad de la clase Linea
     * @param cantidad Variable que simboliza la cantidad de la clase producto
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada producto de la clase Linea
     * @return la variable de retorno simboliza el producto
     */
    public Producto getProducto() {
        return producto;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Producto de la clase Producto
     * @param producto Variable que simboliza el producto de la clase producto
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }

     /**
          * Metodo utilizado para imprimir todos los atributos de la clase Linea en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase Linea en unico string
          */
    public String toString() {
        return "Linea/info{" +
                "numLinea=" + numLinea +"\n"+
                "----------------------------------"+
                ", cantidad=" + cantidad +"\n"+
                "----------------------------------"+
                " producto=" + producto +
                '}';
    }
}