package daniel.rivera.bl;


/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class Producto {
    private String codigo, descripcion;
    private double precio;


    /**
     * Constructor vacio para la clase Producto
     */
    public Producto() {
    }

    /**
     * Constructor que recibe todos los parámetros de  Producto y los Inicializa
     * @param codigo Variable que simboliza el codigo de la clase producto
     * @param descripcion Variable que simboliza - de la clase -
     * @param precio
     */
    public Producto(String codigo, String descripcion, double precio) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    /**
     * Metodo utilizado para modificar el atributo privado cantidad de la clase Producto
     * @param cantidad la cantidad de productos a facturar
     */


    /**
     * metodo utilizado para obtener el valor de la variable privada codigo de la clase Producto
     * @return la variable de retorno simboliza el codigo del producto
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Metodo utilizado para modificar el atributo privado Codigo de la clase Producto
     * @param codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada descripcion de la clase Producto
     * @return la variable de retorno simboliza la descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Metodo utilizado para modificar el atributo privado de descripcion de la clase producto
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * metodo utilizado para obtener el valor de la variable privada precio de la clase Producto
     * @return la variable de retorno simboliza el precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * Metodo utilizado para modificar el atributo privado precio de la clase Producto
     * @param precio Variable que simboliza el de la clase Producto
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

     /**
          * Metodo utilizado para imprimir todos los atributos de la clase - en un unico String
          * @return la variable de retorno simboliza todos los valores de la clase - en unico string
          */
    public String toString() {
        return "Producto{" +
                "codigo='" + codigo + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", precio=" + precio +
                '}';
    }


}