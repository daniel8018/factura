package daniel.rivera.tl;

import daniel.rivera.bl.Cliente;
import daniel.rivera.bl.Factura;
import daniel.rivera.bl.Linea;
import daniel.rivera.bl.Producto;

import java.util.ArrayList;


/**
 * @author Luis Daniel Rivera
 * @version 1.0.1
 * @since 1.0.1
 */
public class CapaLogica {

    private ArrayList<Producto> product;
    private ArrayList<Cliente> client;
    private ArrayList<Factura> fact;

    /**
     * Constructor que recibe todos los parámetros de CapaLogica  y los Inicializa
     */
    public CapaLogica() {
        product = new ArrayList<>();
        client = new ArrayList<>();
        fact = new ArrayList<>();
    }
//__________________________________________________________________

    /**
     * metodo utilizado para registrar el objeto Factura y ser enviado al Array de Factura
     * @param factura Variable que simboliza el objeto factura de la clase Factura
     * @return la variable de retorno simboliza la factura agregada
     */
    public String registrarFacturas(Factura factura) {
        fact.add(factura);
        return null;
    }

    /**
     * metodo utilizado para registrar el objeto Producto y ser enviado a la logica
     * @param producto Variable que simboliza el producto de la clase Producto
     * @return la variable de retorno simboliza el producto agregado
     */
    public String registrarProducto(Producto producto) {
        product.add(producto);
        return null;
    }

    /**
     * metodo utilizado para registrar el objeto Clientes y ser enviado a la capa logica
     * @param clientes Variable que simboliza los clientes de la clase cliente
     * @return la variable de retorno simboliza devuelve null
     */
    public String registrarCliente(Cliente clientes) {
        client.add(clientes);
        return null;
    }
//__________________________________________________________________

    /**
     * Metodo que tiene la funcion de buscar entre el arraylist el parametro
     * @param numero Variable que simboliza el numero de la clase Factura
     * @return la variable de retorno simboliza la factura en caso de ser encontrada
     */
    public Factura encontrarFactura(String numero) {
        for (Factura factura : fact) {
            if (factura.getNumero().equals(numero)) {
                return factura;
            }
        }
        return null;
    }

    /**
     * Metodo que tiene la funcion de buscar entre el arraylist el parametro
     * @param cedula Variable que simboliza la cedula de la clase Cliente
     * @return la variable de retorno simboliza el Cliente en caso de ser encontrado
     */
    public Cliente encontrarCliente(String cedula) {
        for (Cliente cliente : client) {
            if (cedula.equals(cliente.getCedula())) {
                return cliente;
            }
        }
        return null;
    }

    /**
     * Metodo que tiene la funcion de buscar entre el arraylist el parametro
     * @param codigo Variable que simboliza el codigo de la clase Producto
     * @return la variable de retorno simboliza el Producto en caso de ser encontrado
     */
    public Producto encontrarProducto(String codigo) {
        for (Producto producto : product) {
            if (codigo.equals(producto.getCodigo())) {
                return producto;
            }
        }
        return null;
    }
//__________________________________________________________________

    /**
     * metodo que tiene la funcion de listar los clientes de el Arraylist
     * @return la variable de retorno simboliza la informacion
     */
    public String[] listarClientes() {
        String[] info = new String[client.size()];
        for (int i = 0; i < client.size(); i++) {
            info[i] = client.get(i).toString();
        }
        return info;
    }

    /**
     * metodo que tiene la funcion de listar los Productos de el Arraylist
     * @return la variable de retorno simboliza la info
     */
    public String[] mostrarProducto() {
        String[] info = new String[product.size()];
        for (int i = 0; i < product.size(); i++) {
            info[i] = product.get(i).toString();
        }
        return info;
    }

    /**
     * metodo que tiene la funcion de listar las facturas de el Arraylist
     * @return la variable de retorno simboliza la info
     */
    public String[] mostrarFactura() {
        String[] info = new String[fact.size()];
        for (int i = 0; i < fact.size(); i++) {
            info[i] = fact.get(i).toString();
        }
        return info;
    }


//__________________________________________________________________

    /**
     * metodo que busca entre el arraylist el parámetro
     * @param numeroFactura Variable que simboliza el numero de la factura de la clase factura
     * @return la variable de retorno simboliza la posicion
     */
    public int buscarPosFactura(String numeroFactura) {
        int posicion = 0;
        for (Factura factura : fact) {
            if (numeroFactura.equals(factura.getNumero())) {
                return posicion;
            }
            posicion++;
        }
        return -1;
    }

    /**
     * metodo que calcula el subtotal segun el numero de factura
     * @param numeroFactura Variable que simboliza el numero de factura de la clase factura
     * @return la variable de retorno simboliza el subtotal
     */
    public Double calcularSubTotal(String numeroFactura) {
        Linea linea = new Linea();
        Double subTotal = 0.0;
        ArrayList<Linea> info;
        Linea lineaDetalle;
        info = fact.get(buscarPosFactura(numeroFactura)).getLines();
        for (int i = 0; i < info.size(); i++) {
            subTotal += (info.get(i).getCantidad() * info.get(i).getProducto().getPrecio());
        }
        return subTotal;
    }
//__________________________________________________________________

}// FIN CLASE.